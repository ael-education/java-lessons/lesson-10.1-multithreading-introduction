/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.vertexprise.multithreadingintroduction;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author developer
 */
public class PrintProcess implements Runnable {

    private final String name;

    public PrintProcess(String name) {
        this.name = name;
    }
    
  

    @Override
    public void run() {
        int count= 0;
        while (true) {
            try {
                count++;
                
                System.out.println("Привет [" + count + "] от ["+name+"]");
                Thread.sleep(3000);
            } catch (InterruptedException ex) {
                Logger.getLogger(PrintProcess.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
    
}
