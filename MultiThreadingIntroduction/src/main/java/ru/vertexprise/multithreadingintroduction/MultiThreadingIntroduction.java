/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package ru.vertexprise.multithreadingintroduction;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author developer
 */
public class MultiThreadingIntroduction {

    public static void main(String[] args) {
        ExecutorService executor = Executors.newFixedThreadPool(4);
        
        System.out.println("Hello World!");
       
        PrintProcess p1 = new PrintProcess("процесс Вася");
        PrintProcess p2 = new PrintProcess("процесс Петя");
        PrintProcess p3 = new PrintProcess("процесс Коля");
        PrintProcess p4 = new PrintProcess("процесс Ира");
        
        executor.submit(p1);
        executor.submit(p2);
        executor.submit(p3);
        executor.submit(p4);
        
        System.out.println("Все процессы запущены");
    }
}
